\documentclass{beamer}
%\documentclass[handout]{beamer}

\usetheme{AnnArbor}
\usecolortheme{dolphin}
\usefonttheme{professionalfonts}
\usepackage[utf8]{inputenc}
\usepackage[italian]{babel}
\frenchspacing
\usepackage{caption}
\captionsetup[figure]{labelformat=empty,singlelinecheck=off,justification=centering}
\usepackage{tikz}
\usetikzlibrary{calc}

\title[Robot senza memoria]{Formare sequenze di pattern geometrici con robot senza memoria}
\subtitle{Algoritmi paralleli e distribuiti}
\author[Riccardo Macoratti]{Riccardo Macoratti (844553)}
\date{}

\AtBeginSection[] {
  \begin{frame}
    \frametitle{Indice}
    \tableofcontents[currentsection]
  \end{frame}
}
\AtBeginSubsection[]
{
  \begin{frame}
    \frametitle{Table of Contents}
    \tableofcontents[currentsection,currentsubsection]
  \end{frame}
}

\begin{document}
 
\frame{\titlepage}

\begin{frame}
  \frametitle{Indice}
  \tableofcontents
\end{frame}


\section{Introduzione}
\begin{frame}
  \frametitle{Il problema (1)}
  Si analizzano \textbf{sistemi distribuiti} formati da \textbf{robot} semplici ed autonomi, \pause
  che si muovono su un \textbf{piano}, ma \textbf{non possono comunicare} direttamente tra loro \pause
  e sono \emph{oblivious}, cioè \textbf{non possiedono memoria} del passato. \pause \\
  \vspace{.322cm}
  Non c'è un \textbf{coordinatore} centrale.
\end{frame}

\begin{frame}
  \frametitle{Il problema (2)}
  I robot non partono da uno \textbf{stato iniziale} fissato \pause
  e devono disporsi a formare un \emph{pattern} geometrico. \pause
  \vspace{.322cm}
  \begin{block}{Pattern}
    Un \emph{pattern} è un insieme di punti del piano euclideo che delinea una qualche figura geometrica, come un cerchio, una linea o un'altra forma.
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Applicazioni}
  Questi sistemi sono studiati dal punto di vista della robotica, dell'intelligenza artificiale e, soprattutto, del calcolo distribuito. \pause \\
  \vspace{.322cm}
  È stato dimostrato che, sotto certe condizioni, i robot riescono a comporre un singolo pattern, \pause
  ma rimangono da definire i limiti della generazione di \textbf{una serie} di pattern. \pause \\
  Questo porterebbe il sistema, \emph{nel complesso}, ad avere una memoria.
\end{frame}


\section{Robot}
\begin{frame}
  \frametitle{Tipi di robot}
  Si possono caratterizzare tre tipi di robot: \pause
  \begin{itemize}
    \item robot \textbf{identici} e \textbf{anonimi}, e quindi \textbf{indistinguibili} \pause
    \item robot visibilmente \textbf{distinguibili} \pause
    \item robot visibilmente \textbf{indistinguibili}, ma con \textbf{identità distinte}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Proprietà di un robot}
  \begin{columns}
    \column{.2\textwidth}
    \begin{figure}
      \begin{tikzpicture}
        \draw[step=.3cm,gray,very thin] (-1,-1) grid (1,1);
        \draw[fill=black] (0,0) circle (.161cm);
      \end{tikzpicture}
      \caption{\footnotesize{Un robot è un punto sul piano.}}
    \end{figure} \pause
    \column{.8\textwidth}
    Un robot $r$ al tempo $t$:
    \begin{itemize}
      \item ha un sistema di coordinate $Z_{r,t}$ che comprende:
        \begin{itemize} \pause
          \item un insieme di assi cartesiani $x,y$
          \item un'origine che coincide con la posizione corrente
          \item un'unità di distanza
        \end{itemize} \pause
      \item ha dei sensori per determinare la posizione relativa e il numero degli altri robot \pause
      \item esegue un algoritmo per determinare la prossima posizione
    \end{itemize} \pause
    \begin{block}{Sistemi di coordinate}
      Dati due robot, $r$ con $Z_{r,t}$ e $s$ con $Z_{s,t}$, al tempo $t$ è possibile che $Z_{r,t} \neq Z_{s,t}$.
    \end{block}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{SSYNC}
  Un instante di tempo $t$ è detto \emph{round}. Ogni \emph{step}, azione, compiuto da un robot può impiegare più round. \pause
  Tra una fase e l'altra un robot può disattivarsi per un tempo arbitrario. \pause \\
  \vspace{.322cm}
  Si parte dall'assunzione di \emph{semi-sincronizzazione} (\textbf{SSYNC}), cioè che ogni robot attivo completi uno step in un singolo round e, per un'esecuzione infinita, si attivi per infiniti round.
\end{frame}

\begin{frame}
  \frametitle{LOOK-COMPUTE-MOVE}
  Ogni step si divide a sua volta in tre \emph{stage}, per ogni robot $r$ ci sono: \pause
  \begin{enumerate}
    \item \textbf{LOOK}: $r$ costruisce un'istantanea della posizione degli altri robot \pause
    \item \textbf{COMPUTE}: $r$ esegue l'algoritmo per determinare la prossima posizione
    \begin{itemize}
      \item \textbf{input}: i dati dallo stage di LOOK
      \item \textbf{output}: le coordinate della prossima posizione
    \end{itemize} \pause
    \item \textbf{MOVE}: $r$ si muove alla posizione calcolata
  \end{enumerate} \pause
  \begin{block}{Step e coordinate}
    Il sistema di coordinate di un robot può cambiare all'inizio di ogni step.
  \end{block} \pause
  \begin{block}{MOVE}
    Si assume la proprietà di \textbf{rigidità}, cioè che ogni robot raggiunga la sua destinazione in ogni step, a prescindere dalla distanza.
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Notazione (1)}
  Da ora in poi, saranno utilizzate le notazioni presentate. \pause \\
  \vspace{.322cm}
  Siano $n$ il numero di robot nel sistema e sia $r_i$ l'$i$-esimo robot. \pause \\
  \vspace{.322cm}
  Considero come riferimento un sistema di coordinate fisso, in cui:
  \begin{itemize}
    \item la posizione di $r_i$ è $L(r_i) = (x_i, y_i)$
    \item la distanza tra $r_i$ e $r_j$ è $d(r_i,r_j)$, distanza euclidea
  \end{itemize} \pause
  \vspace{.322cm}
  Una configurazione è $\gamma = \{(label(i), L(r_i)) \mid 1 \leq i \leq n\}$, con
  $$label(i) = \begin{cases} i & \mbox{se i robot sono distinti}\\ 1 & \mbox{altrimenti} \end{cases}$$ 
\end{frame}

\begin{frame}
  \frametitle{Notazione (2)}
  Sia $\gamma(t)$ la configurazione al tempo $t$. \pause \\
  \vspace{.322cm}
  Sia $L(\gamma) = \{(x,y) : \exists l, (l,(x,y)) \in \gamma\}$ l'inisieme dei punti della configurazione $\gamma$. \pause \\
  \vspace{.322cm}
  Si definisce il SEC, \emph{smallest enclosing circle}, come la più piccola circonferenza tale che ogni punto sia sulla o all'interno di essa. SEC($\gamma$) è il SEC per l'insieme $L(\gamma)$.
\end{frame}


\section{Pattern, serie e formazione}
\begin{frame}
  \frametitle{Premesse}
  Un \emph{pattern} $P_i$ è un insieme di punti distinti nello spazio euclideo $(x_1,y_1), ..., (x_n,y_n)$, di cardinalità $n_i = size(P_i)$. \pause \\
  \vspace{.322cm}
  Il pattern $P_i$ è \textbf{isomorfo} a $P_j$, $P_i \cong P_j$, se $P_j$ è ottenuto con una \emph{trasformazione piana} da $P_i$, altrimenti $P_i$ è \emph{distinto} da $P_j$. \pause \\
  \begin{block}{Trasformazione piana}
    Le trasformazioni piane sono le operazioni di traslazione, rotazione e scala.
  \end{block} \pause
  \vspace{.322cm}
  Esempi di pattern sono: \pause
  \begin{itemize}
    \item \textbf{POINT}: un singolo punto \pause
    \item \textbf{TWO-POINTS}: l'unico pattern con possibile con due punti \pause
    \item \textbf{POLYGON($k$)}: formato dai $k$ vertici, $p_1, ..., p_k$, di un poligono regolare di $k$ lati 
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Simmetricità}
  \begin{columns}
    \column{.7\textwidth}
    Per ogni pattern $P : size(P) > 1$ esiste SEC($P$). \pause \\
    \vspace{.322cm}
    La \emph{simmetricità} $\rho(P)$ è definita come il maggior intero $q \geq 1$ tale che l'area interna a SEC($P$) possa essere partizionata in $q$ settori circolari congruenti, formando angoli di $\frac{2\pi}{q}$ al centro e $q$ punti, $s_1, ..., s_q$, alla circonferenza. \pause \\
    \vspace{.322cm}
    Per convezione $\rho($POINT$) = \rho(P : size(P) = 1) = \infty$. \pause
    \column{.3\textwidth}
    \begin{figure}
      \begin{tikzpicture}
        \draw[thick] (0,0) circle[radius=1.2];
        \draw
          (0,-1.2)--(0,1.2)
          (-1.2,0)--(1.2,0)
          (-135:1.2)--(45:1.2)
          (135:1.2)--(-45:1.2);
        \draw[fill=red]
          (1.2,0) circle[radius=2pt] node[right] {$s_1$}
          (-45:1.2) circle[radius=2pt] node[below right] {$s_2$}
          (0,-1.2) circle[radius=2pt] node[below] {$s_3$}
          (-135:1.2) circle[radius=2pt] node[below left] {$s_4$}
          (-1.2,0) circle[radius=2pt] node[left] {$s_5$}
          (135:1.2) circle[radius=2pt] node[above left] {$s_6$}
          (0,1.2) circle[radius=2pt] node[above] {$s_7$}
          (45:1.2) circle[radius=2pt] node[above right] {$s_8$};
        \draw[red,thick,domain=0:-45]
          plot ({.36*cos(\x)}, {.36*sin(\x)}) node[right] {\tiny{$\frac{2\pi}{8}{=}\frac{\pi}{4}$}};
      \end{tikzpicture}
      \caption{\footnotesize{Esempio di partizionamento con $q = 8$.}}
    \end{figure}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Tipi di serie di pattern}
  Si considerano due tipi di serie di pattern: \pause
  \begin{itemize}
    \item \emph{serie lineare di pattern}: una sequenza ordinata (possibilmente infinita) $S = \langle P_1, P_2, ...\rangle$ di pattern distinti tra loro. \pause
    \item \emph{serie ciclica ordinata}: una sequenza ordinata e periodica $S^\infty = \langle P_1, ..., P_m\rangle^\infty$ dove $S = \langle P_1, ..., P_m\rangle$ è una serie lineare finita.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Formazione di una serie di pattern (1)}
  Un sistema di robot forma un pattern $P$, se per la configurazione corrente $\gamma$, $L(\gamma) \cong P$. \pause \\
  \vspace{.322cm}
  Un sistema di robot che \pause
  \begin{itemize}
    \item esegue l'algoritmo $\mathcal{A}$ \pause
    \item parte dalla configurazione iniziale $\gamma(t_0)$
  \end{itemize} \pause
  \textbf{forma completamente} una serie di pattern $S = \langle P_1, P_2, ...\rangle$ se \pause
  per ogni possibile esecuzione di $\mathcal{A}$, \pause
  $$
  \exists t_1, t_2, ..., \text{ con } t_0 < t_j < t_{j+1}, \pause
  \text{ tali che } \forall 1 \leq j \leq |S|, L(\gamma(t_j)) \cong P_j
  $$ \pause
  Viceversa, una serie di pattern $S$ si dice \textbf{completamente formabile} se \pause
  esiste un algoritmo $\mathcal{A}$ per un sistema con $k > 0$ robot \pause
  tale che, partendo da una qualsiasi $\gamma(t_0)$, \pause
  il sistema formi completamente $S$.
\end{frame}

\begin{frame}
  \frametitle{Formazione di una serie di pattern (2)}
  \textbf{Lemma 1}. Data una serie lineare finita di pattern $S = \langle P_1, ..., P_m\rangle$ con $m \geq 2$, è dimostrato che nessun algoritmo deterministico $A$ che termina in tempo finito, può formare completamente $S$ da tutte le $\gamma(t_0)$ possibili. \pause \\
  \vspace{.322cm}
  Questo significa un algoritmo che termina, al più, può solo formare un suffisso di $S$. \pause \\
  \vspace{.322cm}
  È possibile, però, costruire un algoritmo che non termina, ma forma la serie $S$; questo si verifica se $S^\infty$ è completamente formabile. \\
  \vspace{.161cm}
  Per questo motivo ci si concentrerà solo sulle sequenze cicliche.
\end{frame}


\section{Robot anonimi}
\subsection{Premesse}
\begin{frame}
  \frametitle{Vista centrale (1)}
  \textbf{Definizione 1}. La \emph{vista centrale} di un robot $r$ al tempo $t$ è definita, rispetto al sistema di coordinate $Z_{r,t}$, come un insieme di tuple $CV_r(t) = \{(x_i,y_i,k_i) \mid k_i > 0 \text{ robot posizionati in } (x_i,y_i)\}$. \pause \\
  \vspace{.322cm}
  \begin{example}
    $$
    \{(0,0,2), (1,0,1), (1,1,1), ...\}
    $$
    $$
    \begin{bmatrix} 
      x_{0,0} & x_{0,1} & \dots  & x_{0,n-1} \\
      x_{1,0} & x_{1,1} & \dots  & x_{1,n-1} \\
      \vdots & \vdots & \ddots & \vdots \\
      x_{n-1,0} & x_{n,1} & \dots  & x_{n-1,n-1}
    \end{bmatrix}
    $$
    Visibile come una matrice memorizzata in maniera sparsa.
  \end{example}
\end{frame}

\begin{frame}
  \frametitle{Vista centrale (2)}
  È importante costruire un sistema di coordinate indipendente dai sistemi locali. \pause \\
  Se $\gamma$ è la configurazione corrente, ci sono due casi: \pause
  \begin{itemize}
    \item $r_i$ non è posizionato nel centro $c$ del SEC($\gamma$) $\Longrightarrow$ $L(r_i) = (0,0)$ e il centro $c$ si posiziona a $(1,0)$. \pause
    \item $r_i$ è posizionato nel centro $c$ del SEC($\gamma$) $\Longrightarrow$ $L(r_i) = (0,0)$, mentre $L(r_j)$, tale che $CV_{r_j}(t)$ è minima verso tutti gli altri robot, si posiziona a $(1,0)$.
  \end{itemize} \pause
  \vspace{.322cm}
  In questo modo $L(r_i) = L(r_j) \Longrightarrow CV(r_i) = CV(r_j)$.
\end{frame}

\begin{frame}
  \frametitle{Raggiungibilità di una configurazione}
  Una configurazione $\gamma$ contiene robot anonimi che occupano al più $w$ posizioni distinte. $\gamma'$ con $w' > w$ posizioni occupate \textbf{non è raggiungibile}. \pause \\
  \vspace{.322cm}
  Per questo motivo, una serie di pattern formabile comprenderà solo pattern isomorfi a configurazioni con lo stesso numero $w$ di posizioni occupate distinte.
\end{frame}

\begin{frame}
  \frametitle{Simmetricità con vista centrale (1)}
  \textbf{Definizione 2}. Data una configurazione $\gamma$ di $n$ robot, la simmetricità, $\rho(\gamma)$, è il maggior intero $q$ tale che per ogni robot $r$, ci sia un multiplo di $q$ robot che hanno la sua stessa vista.
  $$
  \rho(\gamma) = max\{q : \forall r_i \in \gamma, q \text{ divide } |\{r_j \in \gamma : CV(r_j) = CV(r_i)\}|\}
  $$ \pause
  Se ne deduce che: \pause
  \begin{itemize}
    \item \textbf{Proprietà 1}. $\forall \gamma$, $\rho(\gamma) \text{ divide } n$ \pause
    \item \textbf{Proprietà 2}. $\forall \gamma$, dove i robot occupano posizioni \textbf{distinte}:
      \begin{itemize}
        \item se è presente un robot al centro di SEC($\gamma$), $\rho(\gamma) = 1$
        \item se $\rho(\gamma) = q > 1$, i robot sono ai vertici di poligoni regolari di $q$ lati, concentrici a SEC($\gamma$)
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Simmetricità e vista centrale (2)}
  \begin{columns}
    \column{.33\textwidth}
      \begin{figure}
        \includegraphics{plots/simmetricity1.pdf}
        \caption{\footnotesize{$\rho(\gamma) = 1$, i robot hanno viste distinte.}}
      \end{figure} \pause
    \column{.33\textwidth}
      \begin{figure}
        \includegraphics{plots/simmetricity2.pdf}
        \caption{\footnotesize{$\rho(\gamma) = 1$, un solo robot ha una vista differente.}}
      \end{figure} \pause
    \column{.33\textwidth}
      \begin{figure}
        \includegraphics{plots/simmetricity3.pdf}
        \caption{\footnotesize{$\rho(\gamma) = 4$.}}
      \end{figure}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Simmetricità e vista centrale (3)}
  \textbf{Lemma 2}. Si dimostra che se la configurazione corrente $\gamma$ ha simmetricità $\rho(\gamma) = q$, \pause
  allora, per qualsiasi algoritmo, deve esistere un'esecuzione dove per tutte le configurazioni successive $\gamma'$, deve valere $\rho(\gamma') = l \cdot q$, per qualche $l \geq 1$.
\end{frame}

\subsection{Robot che partono da posizioni distinte}
\begin{frame}
  \frametitle{Formabilità di una serie di pattern}
  \textbf{Lemma 3}. Una serie ciclica di pattern distinti $S^\infty = \langle P_1, ..., P_m\rangle^\infty$ è \emph{formabile}, $\forall i,j \in \{1, 2, ..., m\}$, se: \pause
  \begin{itemize}
    \item $size(P_i) = size(P_j)$ \pause
    \item $\rho(P_i) = \rho(P_j)$
  \end{itemize} \pause
  \vspace{.322cm}
  \begin{block}{Nota}
  Esiste un solo pattern $P$ tale che $\rho(P) = size(P) = n$, POLYGON($n$), perciò una serie di pattern non banale può essere formata solo con configurazioni $\gamma$ tali che $\rho(\gamma) < n$.
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Configurazioni notevoli}
  L'algoritmo, utile a formare una serie di pattern, si basa sul riconoscimento di due configurazioni particolari, partendo da una configurazione qualsiasi. \pause
  \begin{figure}
    \includegraphics[width=.3\textwidth]{plots/configuration1.pdf}
    \caption{\footnotesize{Una configurazione asimmetrica $\gamma$, con il SEC($\gamma$).}}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Bi-circular configuration (BCC)}
  \textbf{Definizione 3}. Una configurazione è bi-circolare se: \pause
  \begin{itemize}
    \item esiste una posizione \emph{pivot}, tale che, se $\mathcal{C}_1$ è il SEC di tutti i robot e $\mathcal{C}_2$ quello di tutti i robot meno quelli al pivot, allora $diametro(\mathcal{C}_1) \geq 3 \cdot diametro(\mathcal{C}_2)$. $3$ è detto periodo. \pause
    \item $\mathcal{C}_1 \cap \mathcal{C}_2 = \{b_p\}$, con $b_p$ \emph{base-point}, il punto opposto al pivot.
  \end{itemize} \pause
  \begin{figure}
    \includegraphics[width=.4\textwidth]{plots/configuration2.pdf}
    \caption{\footnotesize{Una configurazione bi-circolare, formata spostando uno dei robot.}}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{$q$-simmetric-circular configuration (SCC($q$))}
  \textbf{Definizione 4}. $\gamma$ è $q$-simmetrica-circolare, $1 < q < n$, se:
  \begin{itemize}
    \item non ci sono robot al centro di $\mathcal{C}_1$. \pause
    \item ci sono esattamente $q$ robot sul perimetro di $\mathcal{C}_1$ formando POLYGON($q$). \pause
    \item gli altri robot sono contenuti nel cerchio $\mathcal{C}_3$, tale che $diametro(\mathcal{C}_1) \geq (5 + sin^{-1}(\frac{\pi}{q})) \cdot diametro(\mathcal{C}_3)$. $5 + sin^{-1}(\frac{\pi}{q})$ è detto periodo.
  \end{itemize} \pause
  \begin{figure}
    \includegraphics[width=.3\textwidth]{plots/configuration3.pdf}
    \caption{\footnotesize{Una configurazione $q$-simmetrica-circolare, con $q=4$.}}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Generalizzazione}
  \textbf{Lemma 4}. Partendo da una configurazione $\gamma$, con simmetricità $\rho(\gamma) = q$, $q < n$, \pause
  e $\forall k \geq (5 + sin^{-1}(\frac{\pi}{q}))$, \pause
  è possibile raggiungere una configurazione $\gamma'$ tale che: \pause
  \begin{itemize}
    \item o $\gamma'$ è una SCC($q'$), $q' > 1$ e $q' = q \cdot l$, con $k$ come periodo. \pause
    \item o $\gamma'$ è una BCC, con $k' = \frac{k+1}{2}$ come periodo.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Configurazioni SCC}
  \textbf{Lemma 5}.  Partendo da una configurazione di tipo SCC($q$), con $q > 1$, \pause
  con $n$ robot che occupano posizioni distinte, \pause
  è possibile formare qualsiasi pattern che abbia \pause
  \begin{itemize}
    \item $size(P) = n$ \pause
    \item $\rho(P) = q \cdot a$ per qualche $a \geq 1$.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Configurazioni BCC}
  \textbf{Lemma 6}. Considerando una configurazione di tipo BCC, con $n \geq 4$ robot in posizioni distinte, \pause
  è possibile formare qualsiasi pattern $P$ di dimensione $n$. \pause
  \begin{block}{Dimensione della BCC}
    È fondamentale che $n \geq 4$ e che i robot siano in posizioni distinte.
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Algoritmo per formare una serie di pattern (1)}
  Prendendo in considerazione i risultati mostrati, si può descrivere un algoritmo per formare una serie ciclica di pattern distinti  $S^\infty = \langle P_1, ..., P_m\rangle^\infty$, \pause
  con $n$ robot che partono da una configurazione $\gamma$, \pause
  ponendo che $\forall i$ $size(P_i) = n$ e $\rho(P_i) = q = \rho(\gamma)$. \pause \\
  \vspace{.322cm}
  Ci sono diversi casi da considerare: \pause
  \begin{itemize}
    \item $q = n$ o $n = 2$: c'è un solo possibile pattern, POLYGON($n$) e i robot già formano questo pattern (Lemma 3). \pause
    \item $q = 2$, $n = 3$: è impossibile poiche $q$ non divide $n$ (Proprietà 1). \pause
    \item $q = 1$, $n = 3$: ogni avanzamento di configurazione richiede lo spostamento di un solo robot e questo è possibile poichè $\rho(\gamma) = 1$.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Algoritmo per formare una serie di pattern (2)}
  \begin{itemize}
    \item $q < n$ e $n \geq 4$: \pause \\
    \vspace{.322cm}
    Si definisce $F: S \longmapsto \mathbb{R}$ una funzione che mappa ogni pattern $P_i \in S$ in un numero reale $k_i$, tale che $k_i = F(P_i)$. \pause \\
    \vspace{.322cm}
    Per segnalare la formazione di un pattern  viene usata \pause
    o una configurazione SCC($x$) con periodo $k_i$, con $q = l \cdot x$ \pause
    o una configurazione BCC con periodo $k_i' = \frac{k_i+1}{2}$. \pause \\
    \vspace{.322cm}
    Grazie al Lemma 4 è possibile formare qualsiasi pattern partendo da una configurazione $\gamma$ con $\rho(\gamma) = q$ e calcolandone il periodo è possibile capire quale pattern $P_i$ sarà formato. \pause \\
    Per i Lemmi 5 e 6, è possibile formare $P_i$ con simmetricità $q$. \pause \\
    \vspace{.322cm}
    Riapplicando il Lemma 4, si ritorna ad una SCC o BCC con periodo $k_{i+1} = F(P_{i+1})$ e si può formare $P_{i+1}$ (di nuovo Lemmi 5 e 6). 
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Algoritmo per formare una serie di pattern (3)}
  Tramite l'algoritmo appena descritto, e applicando ricorsivamente i Lemmi 4, 5 e 6, si può formare la sequenza di pattern ciclica. È possibile perciò dedurre il seguente teorema: \pause
  \vspace{.322cm}
  \begin{block}{Teorema 1}
    Un insieme di $n$ robot che partono da posizioni distinte, posti in un'arbitraria configurazione $\gamma$, \pause
    possono formare una serie ciclica di pattern distinti di dimensione $n$, $S^\infty = \langle P_1, ..., P_m\rangle^\infty$, \pause
    se, $\forall i,j \in \{1, ..., m\}$, \pause
    $\rho(P_i) = \rho(P_j) = a \cdot \rho(\gamma)$ per qualche $a \geq 1$.
  \end{block}
  \vspace{.322cm}
  È quindi dimostrato che sotto determinate condizioni, partendo da una configurazione iniziale qualunque, è possibile formare una serie di pattern.
\end{frame}


\section*{Ringraziamenti}
\begin{frame}
  \begin{center}
    \Huge{Grazie!}
  \end{center}
\end{frame}


\section*{Bibliografia}
\begin{frame}
  \frametitle{Bibliografia}    
  \begin{thebibliography} 
    \beamertemplatearticlebibitems
    \bibitem{Das2014}
      Das, Shantanu, Paola Flocchini, Nicola Santoro, and Masafumi Yamashita.
      \newblock "Forming sequences of geometric patterns with oblivious mobile robots."
      \newblock Distributed Computing 28, no. 2 (2014): 131-45. doi:10.1007/s00446-014-0220-9.
  \end{thebibliography}
\end{frame}

\end{document}